# Demo of non-blocking vs blocking  
This application is meant to be run together with the Producer app. Consumer will query Producer and when it receives data it will make some supernecessary, time-consuming, enhancement. 

## Traditional, blocking  
Triggered with:  
`curl http://localhost:8090/startold`
This will trigger consumer to ask producer in a tradional approach, fetching all data before starting the enhancement. When finished the log will present how long the operation took.

## Webflux, non-blocking  
Triggered with:  
`curl http://localhost:8090/start`
This will trigger consumer to ask producer using Webflux, starting enhancement directly when the first value is received as a stream. When finished the log will present how long the operation took.

# Build JAR  
Build the application with 
`mvn clean package`

# Run application  
`java -jar producer-0.0.1-SNAPSHOT.jar`
