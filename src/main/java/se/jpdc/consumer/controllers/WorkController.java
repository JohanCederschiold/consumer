package se.jpdc.consumer.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import se.jpdc.consumer.services.ConsumerService;

@RestController

public class WorkController {
    ConsumerService service;

    @Autowired
    public WorkController(ConsumerService service) {
        this.service = service;
    }

    @GetMapping("/start")
    public void startWork () {
        service.getSuperImportantData();
    }

    @GetMapping("/startold")
    public void startOldWork () {
        service.getSuperImportantDataByOldEndpoint();
    }
}
