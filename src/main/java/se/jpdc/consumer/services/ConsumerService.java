package se.jpdc.consumer.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Instant;
import java.util.Objects;
import java.util.function.UnaryOperator;

@Service
public class ConsumerService {

    Logger logger = LoggerFactory.getLogger(ConsumerService.class);

    WebClient client;

    public ConsumerService (WebClient.Builder builder) {
        client = builder.baseUrl("http://localhost:8080").build();
    }

    public void getSuperImportantData() {
        long start = Instant.now().toEpochMilli();
        client.get()
                .uri("/data")
                .retrieve()
                .bodyToFlux(String.class)
                .map(superNecessaryEnhancenment)
                .doOnNext(s -> logger.info(s))
                .doOnComplete(() -> logger.info(String.format("I have received all data. Time: %d", (Instant.now().toEpochMilli() - start))))
                .subscribe();
    }

    @SuppressWarnings("squid:S2629")
    public void getSuperImportantDataByOldEndpoint() {
        RestTemplate restTemplate = new RestTemplate();
        long start = Instant.now().toEpochMilli();
        ResponseEntity <String[]> response = restTemplate.getForEntity("http://localhost:8080/data/old", String[].class);
        for (String s : Objects.requireNonNull(response.getBody())) {
            logger.info(superNecessaryEnhancenment.apply(s));
        }
        logger.info(String.format("I have received all data. Time: %d", (Instant.now().toEpochMilli() - start)));
    }

    private final UnaryOperator<String> superNecessaryEnhancenment = s -> {
        String now = "";
        while (!now.endsWith(s)) {
            now = Long.toString(Instant.now().toEpochMilli());
        }
        return String.format("Approved:%s", s);
    };
}
